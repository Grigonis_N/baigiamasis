﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Darbas.Models
{
    public class DropDown
    {
        public Klausima kls{get;set;}
        public Atsakyma ats { get; set; }
    }
}