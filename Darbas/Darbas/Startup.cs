﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Darbas.Startup))]
namespace Darbas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
