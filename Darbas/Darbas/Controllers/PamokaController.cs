﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Darbas.Views.Kategorija
{
    public class PamokaController : Controller
    { 
        DuombazeEntities db = new DuombazeEntities();
        private bool exists = false;
        Pamoka temp;
        // GET: Pamoka

        [HttpGet]
        public ActionResult Sarasas(int id)
        {
            var da = db.Pamokas.Where(pam => pam.KategorijosID == id);
            return View(da);
        }

        [HttpGet]
        public ActionResult Detales(int id)
        {
            Pamoka pamoka = db.Pamokas.Single(pam => pam.Id == id);
            return View(pamoka);
        }

       

    }
}