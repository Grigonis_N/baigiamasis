﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Darbas.Models;

namespace Darbas.Controllers
{
    //[Authorize(Roles = "A")]
    public class AdminController : Controller
    {
        private DuombazeEntities db = new DuombazeEntities();
        private bool exists = false;
        Pamoka temp;
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        #region Pamoka

        // GET: Pamoka
        [HttpGet]
        public ActionResult Prideti()
        {
            IEnumerable<SelectListItem> items = db.Kategorijas.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.KategorijosPavadinimas
            });
            ViewData["JobTitle"] = items;
            return View();
        }

        [HttpPost, ActionName("Prideti")]
        public ActionResult PridetiPatvirtinimas(Pamoka pamoka, HttpPostedFileBase Failas)
        {
            IEnumerable<SelectListItem> items = db.Kategorijas.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.KategorijosPavadinimas
            });
            ViewData["JobTitle"] = items;
            if (ModelState.IsValid)
            {
                if (pamoka.Pavadinimas != null)
                {
                    if (Failas != null)
                    {
                        var id = db.Pamokas.ToList().Count;
                        pamoka.Id = id + 1;
                        string withOutSpecialCharacters =
                            new string(Failas.FileName.Where(c => char.IsLetterOrDigit(c) || c == '.').ToArray());
                        foreach (var pam in db.Pamokas)
                        {
                            if (pam.FailoPavadinimas.Equals(withOutSpecialCharacters))
                            {
                                exists = true;
                                break;
                            }
                        }
                        if (!exists)
                        {
                            Process(Failas);
                            pamoka.FailoPavadinimas = withOutSpecialCharacters;
                            if (pamoka.VideoNuoroda != null)
                            {
                                var a = CheckIfEmbed(pamoka.VideoNuoroda);
                                pamoka.VideoNuoroda = a;
                            }
                            foreach (Darbas.Kategorija KAT in db.Kategorijas)
                            {
                                if (pamoka.KategorijosID == KAT.Id)
                                {
                                    KAT.PamokuSkaicius++;
                                }
                                db.Kategorijas.AddOrUpdate(KAT);
                            }
                            db.Pamokas.Add(pamoka);
                            db.SaveChanges();
                            TempData["Ikelta"] = "Pamoka sekmingai prideta! Galite grizti i sarasa..";
                        }
                        else
                        {
                            TempData["Failas"] = "Failas egzistuoja tokiu vardu.";
                        }
                    }
                    else
                    {
                        TempData["Nepasirinktas"] = "Nepasirinktas joks failas";
                    }
                }
                else
                {
                    TempData["Klaida"] = "Nepavyko issaugoti pamokos.";
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult AdminSarasas()
        {
            var da = db.Pamokas.ToList();
            return View(da);
        }

        [HttpGet]
        public ActionResult Redaguoti(int id)
        {
            IEnumerable<SelectListItem> items = db.Kategorijas.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.KategorijosPavadinimas
            });
            ViewData["JobTitle"] = items;
            Pamoka pamoka = db.Pamokas.Single(pam => pam.Id == id);

            return View(pamoka);
        }

        [HttpPost, ActionName("Redaguoti")]
        [ValidateAntiForgeryToken]
        public ActionResult RedaguotiPatvirtinimas(Pamoka pamoka)
        {
            temp = db.Pamokas.Single(pam => pam.Id == pamoka.Id);
            IEnumerable<SelectListItem> items = db.Kategorijas.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.KategorijosPavadinimas
            });
            ViewData["JobTitle"] = items;
            if (ModelState.IsValid)
            {
                if (temp.KategorijosID != pamoka.KategorijosID)
                {
                    var id = temp.KategorijosID;
                    var kategorija = db.Kategorijas.Single(kat => kat.Id == id);
                    kategorija.PamokuSkaicius = kategorija.PamokuSkaicius - 1;
                    db.Kategorijas.AddOrUpdate(kategorija);
                }
                db.Pamokas.AddOrUpdate(pamoka);
                db.SaveChanges();
                TempData["Ikelta"] = "Sekmingai atnaujinta";
            }
            else
            {
                TempData["Klaida"] = "Ivyko klaida.";
            }
            return View();
        }

        [HttpGet]
        public ActionResult Trinti(int id)
        {
            Pamoka pamoka = db.Pamokas.Single(pam => pam.Id == id);
            return View(pamoka);
        }

        [HttpPost, ActionName("Trinti")]
        [ValidateAntiForgeryToken]
        public ActionResult TrintiPatvirtinimas(int id)
        {
            Pamoka pamoka = db.Pamokas.Find(id);
            var fileName = pamoka.FailoPavadinimas;
            var fullPath = Server.MapPath("~/Pamoka/Detales/" + fileName);
            temp = pamoka;
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
                ViewBag.deleteSuccess = "true";
            }
            var kategorijosID = temp.KategorijosID;
            var kategorija = db.Kategorijas.Single(kat => kat.Id == kategorijosID);
            kategorija.PamokuSkaicius = kategorija.PamokuSkaicius - 1;
            db.Kategorijas.AddOrUpdate(kategorija);
            db.Pamokas.Remove(pamoka);
            db.SaveChanges();
            return RedirectToAction("AdminSarasas");
        }

        private bool isValid(string contentType)
        {
            return contentType.Equals("application/pdf");
        }

        private bool isValidLength(int contentLength)
        {
            return ((contentLength / 1024) / 1024 < 1);
        }

        public void Process(HttpPostedFileBase Failas)
        {
            if (!isValid(Failas.ContentType))
            {
                TempData["PDF"] = "Tik PDF failai";
            }
            else if (!isValidLength(Failas.ContentLength))
            {
                TempData["Dydis"] = "Too large";
            }
            else
            {
                if (Failas.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(Failas.FileName);
                    string withOutSpecialCharacters = new string(fileName.Where(c => char.IsLetterOrDigit(c) || c == '.').ToArray());
                    var path = Path.Combine(Server.MapPath("~/Pamoka/Detales/"), withOutSpecialCharacters);
                    Failas.SaveAs(path);
                    ViewBag.fileName = fileName;
                }
            }
        }

        public string CheckIfEmbed(string url)
        {
            if (url.Contains("youtube.com/embed/"))
            {
            }
            else
            {
                url.Replace("youtube.com", "youtube.com/embed/");
            }
            return url;
        }
        #endregion

        #region Kategorija

        [HttpGet]
        public ActionResult Kategorijos()
        {
            List<Kategorija> kategorijos = db.Kategorijas.ToList();
            return View(kategorijos);
        }


        [HttpGet]
        public ActionResult PridetiKategorija()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TrintiKategorija(int id)
        {
            Kategorija kategorija = db.Kategorijas.Single(kat => kat.Id == id);
            return View(kategorija);
        }

        [HttpPost, ActionName("TrintiKategorija")]
        [ValidateAntiForgeryToken]
        public ActionResult TrintiKategorijaPatvirtinimas(int id)
        {
            Kategorija category = db.Kategorijas.Find(id);
            db.Kategorijas.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult PridetiKategorija(Kategorija kategorija)
        {
            bool exists = false;
            if (ModelState.IsValid)
            {
                if (kategorija.KategorijosPavadinimas != null)
                {
                    foreach (var cat in db.Kategorijas)
                    {
                        if (cat.KategorijosPavadinimas.Equals(kategorija.KategorijosPavadinimas))
                        {
                            exists = true;
                            break;
                        }
                    }
                    if (!exists)
                    {
                        var id = db.Kategorijas.ToList().Count;
                        kategorija.Id = id + 1;
                        kategorija.PamokuSkaicius = 0;
                        db.Kategorijas.Add(kategorija);
                        var resp = db.SaveChanges();
                        TempData["Success"] = "Kategorija sekmingai prideta! Galite grizti i sarasa..";
                    }
                    else
                    {
                        TempData["Error"] = "Tokiu pavadinimu kategorija jau egzistuoja!";
                    }
                }
            }
            return View(kategorija);
        }

        #endregion

        #region Testas

        [HttpGet]
        public ActionResult PridetiTesta()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem
            {
                Text = "Pasirinkite Kategorija",
                Value = "-1",
                Selected = true
            });

            foreach (var kat in db.Kategorijas)
            {
                items.Add(new SelectListItem { Text = kat.KategorijosPavadinimas, Value = kat.Id.ToString() });
            }

            ViewBag.CategoryType = items;

            return View();
        }

        public JsonResult GetSubCategories(string id)
        {
            List<SelectListItem> subCat = new List<SelectListItem>();

            subCat.Add(new SelectListItem
            {
                Text = "Select",
                Value = "-1"
            });
            foreach (var kat in db.Pamokas)
            {
                if (kat.KategorijosID.ToString().Equals(id))
                {
                    subCat.Add(new SelectListItem { Text = kat.Pavadinimas, Value = kat.Id.ToString() });
                }
            }
            return Json(new SelectList(subCat, "Value", "Text"));
        }

        [HttpPost]
        public ActionResult PridetiTesta(FormCollection form)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem
            {
                Text = "Pasirinkite kategorija",
                Value = "-1",
                Selected = true
            });

            foreach (var kat in db.Kategorijas)
            {
                items.Add(new SelectListItem { Text = kat.KategorijosPavadinimas, Value = kat.Id.ToString() });
            }

            List<string> klsIds = new List<string>();
            List<string> atsIds = new List<string>();

            ViewBag.CategoryType = items;
            if (items != null)
            {
                int katselectedItem = Convert.ToInt32(form["CategoryType"]);

                if (katselectedItem.ToString().Equals("-1"))
                {
                    TempData["nerakategorijos"] = "Nepasirinkta kategorija";
                }
                else
                {
                    int pamselectedItem = Convert.ToInt32(form["SubCategory"]);

                    if (pamselectedItem.ToString().Equals("-1"))
                    {
                        TempData["nerapamokos"] = "Nepasirinkta pamoka";
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            //ViewBag.country_id = new SelectList(db.Kategorijas, "country_id", "countryName", kategorija.Id);
                            string[] klausimas = form["klausimas"].Split(char.Parse(","));
                            string[] atsakymas = form["atsakymas"].Split(char.Parse(","));
                            string[] teisingas = form["teisingas"].Split(char.Parse(","));

                            string[,] answers = new string[10, 3];
                            int countAnswers = 0;
                            for (int i = 0; i <= klausimas.Length - 1; i++)
                            {
                                if (!klausimas[i].Equals(string.Empty))
                                {
                                    int d = 0;
                                    if (countAnswers != 0)
                                    {
                                        countAnswers = countAnswers + 1;
                                    }
                                    for (int j = countAnswers; j <= atsakymas.Length - 1; j++)
                                    {
                                        if (!atsakymas[j].Equals(""))
                                        {
                                            answers[i, d] = atsakymas[j];
                                            if (d == 2)
                                            {
                                                break;
                                            }
                                            d++;
                                            countAnswers++;
                                        }
                                    }
                                }
                            }

                            for (int i = 0; i <= klausimas.Length - 1; i++)
                            {
                                if (!klausimas[i].Equals(string.Empty))
                                {
                                    Klausima kls = new Klausima();
                                    kls.Id = db.Klausimas.ToList().Count + 1;
                                    klsIds.Add(kls.Id.ToString());
                                    kls.KategorijosID = katselectedItem;
                                    kls.PamokosID = pamselectedItem;
                                    kls.Klausimas = klausimas[i];
                                    db.Klausimas.Add(kls);
                                    for (int j = 0; j <= 2; j++)
                                    {
                                        if (!atsakymas[j].Equals(""))
                                        {
                                            Atsakyma ats = new Atsakyma();
                                            ats.Id = db.Atsakymas.ToList().Count + 1;
                                            atsIds.Add(ats.Id.ToString());
                                            ats.Atsakymas = answers[i, j];
                                            ats.KlausimoID = kls.Id;
                                            if (Convert.ToInt32(teisingas[i]) == j + 1)
                                            {
                                                ats.Teisingas = true;
                                            }
                                            db.Atsakymas.Add(ats);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            TempData["Atsakymas"] = "Neivesti visi atsakymai.";
                                        }
                                    }
                                    db.SaveChanges();
                                }
                                else
                                {
                                    TempData["Klausimas"] = "Neivestas klausimas.";
                                }
                            }
                            ViewData["rows"] = klausimas.Length;
                        }
                    }
                }
            }

            return View();
        }

        #endregion

        #region Praktika

        [HttpGet]
        public ActionResult PridetiPraktika()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem
            {
                Text = "Pasirinkite Kategorija",
                Value = "-1",
                Selected = true
            });

            foreach (var kat in db.Kategorijas)
            {
                items.Add(new SelectListItem { Text = kat.KategorijosPavadinimas, Value = kat.Id.ToString() });
            }

            ViewBag.CategoryType = items;
            return View();
        }

        [HttpPost, ActionName("PridetiPraktika"), ValidateInput(false)]
        public ActionResult PridetiPraktikaConfirmed(FormCollection form)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem
            {
                Text = "Pasirinkite Kategorija",
                Value = "-1",
                Selected = true
            });

            foreach (var kat in db.Kategorijas)
            {
                items.Add(new SelectListItem { Text = kat.KategorijosPavadinimas, Value = kat.Id.ToString() });
            }

            ViewBag.CategoryType = items;
            if (items != null)
            {
                int katselectedItem = Convert.ToInt32(form["CategoryType"]);

                if (katselectedItem.ToString().Equals("-1"))
                {
                    TempData["nerakategorijos"] = "Nepasirinkta kategorija";
                }
                else
                {
                    int pamselectedItem = Convert.ToInt32(form["SubCategory"]);

                    if (pamselectedItem.ToString().Equals("-1"))
                    {
                        TempData["nerapamokos"] = "Nepasirinkta pamoka";
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            //ViewBag.country_id = new SelectList(db.Kategorijas, "country_id", "countryName", kategorija.Id);
                            string[] klausimas = form["variantas"].Split(char.Parse(","));
                            List<string> ids = new List<string>();

                            Praktika praktika = new Praktika();
                            praktika.Id = db.Praktikas.Count() + 1;
                            praktika.KategorijosID = katselectedItem;
                            praktika.PamokosID = pamselectedItem;
                            foreach (var variant in klausimas)
                            {
                                Variantai variantas = new Variantai();
                                variantas.Id = db.Variantais.Count() + 1;
                                variantas.PraktikosID = praktika.Id;
                                string kazkas = variant.ToString().Replace("\r\n", "<br/>");
                                variantas.Variantas = kazkas.ToString();
                                ids.Add(variantas.Id.ToString());
                                db.Variantais.Add(variantas);
                                db.SaveChanges();
                            }
                            StringBuilder builder = new StringBuilder();
                            foreach (string id in ids) // Loop through all strings
                            {
                                builder.Append(id).Append("$"); // Append string to StringBuilder
                            }
                            string result = builder.ToString(); // Get string from StringBuilder
                            praktika.Variantai = result;
                            db.Praktikas.Add(praktika);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return View();
        }

        #endregion
    }
}