﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Darbas.Controllers
{
    public class TestasController : Controller
    {
        private DuombazeEntities db = new DuombazeEntities();
        [HttpGet]
        public ActionResult Detales(int id)
        {
            List<Klausima> klausimas = new List<Klausima>();
            List<Atsakyma> atsakymai = new List<Atsakyma>();
            ViewBag.Pasirinkta = id;
            foreach (Klausima kls in db.Klausimas)
            {
                if (kls.PamokosID == id)
                {
                    klausimas.Add(kls);
                    foreach (Atsakyma ats in db.Atsakymas)
                    {
                        if (ats.KlausimoID == kls.Id)
                        {
                            atsakymai.Add(ats);
                        }
                    }
                }
            }
            ViewBag.Kls = klausimas;
            ViewBag.Ats = atsakymai;
            ViewBag.True = null;
           return View();
        }
        [HttpPost, ActionName("Detales")]
        public ActionResult DetalesConfirm(FormCollection form, int id)
        {
            List<Klausima> klausimas = new List<Klausima>();
            List<Atsakyma> atsakymai = new List<Atsakyma>();
            ViewBag.Pasirinkta = id;
            foreach (Klausima kls in db.Klausimas)
            {
                if (kls.PamokosID == id)
                {
                    klausimas.Add(kls);
                    foreach (Atsakyma ats in db.Atsakymas)
                    {
                        if (ats.KlausimoID == kls.Id)
                        {
                            atsakymai.Add(ats);
                        }
                    }
                }
            }
            ViewBag.Kls = klausimas;
            ViewBag.Ats = atsakymai;

            List<int> atsakymas = new List<int>();
            foreach (var kat in klausimas)
            {
                var a = form[kat.Id.ToString()];
                 atsakymas.Add(Convert.ToInt32(a));
            }
            int teisingas = 0;

            foreach (var ats in atsakymas)
            {
                foreach (var atsakyma in db.Atsakymas)
                {
                    if (atsakyma.Id == ats)
                    {
                        if (atsakyma.Teisingas == true)
                        {
                            teisingas++;
                        }
                        break;
                    }
                }
            }
            var aaa = User.Identity.Name;
            ViewBag.True = "Rezultatas: " + teisingas.ToString() + '/' + klausimas.Count.ToString();
            return View();
            ViewBag.True = null;
        }
    }
}
