﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Darbas.Controllers
{
    public class PraktikaController : Controller
    {
        DuombazeEntities db = new DuombazeEntities();
        
     
        // GET: Praktika
        [HttpGet, ValidateInput(false)]
        public ActionResult Detales(int id)
        {
            var pr = db.Praktikas.Single(x => x.PamokosID == id);

            var ids = pr.Variantai.Split('$');

            var variants = db.Variantais.ToList();
            
            List<string> a = new List<string>();
            foreach (var variant in variants)
            {
                foreach (var aidizas in ids)
                {
                    if (aidizas.Equals(variant.Id.ToString()))
                    {
                        a.Add(variant.Variantas);
                    }
                }
                
            }
            List<string> b = new List<string>();
            StringBuilder builder = new StringBuilder();
            foreach (string aa in a) // Loop through all strings
            {
                builder.Append(aa).Append("|"); // Append string to StringBuilder
            }
            string result = builder.ToString(); // Get string from StringBuilder
            string result1 = "";
            do
            {
                builder.Clear();
                Random rand = new Random();
                b = a.OrderBy(c => rand.Next()).ToList();
                foreach (string bb in b) // Loop through all strings
                {
                    builder.Append(bb).Append("|"); // Append string to StringBuilder
                }
                result1 = builder.ToString(); // Get string from StringBuilder
            }
            while (result.Equals(result1));
             
            ViewBag.Kls = b;
            ViewBag.Id = id;
            return View();
        }
        [HttpPost, ActionName("Detales"), ValidateInput(false) ]
        public ActionResult DetalesConfirmed(FormCollection form, int id)
        {
            var pr = db.Praktikas.Single(x => x.PamokosID == id);

            var ids = pr.Variantai.Split('$');

            var variants = db.Variantais.ToList();

            List<string> a = new List<string>();
            foreach (var variant in variants)
            {
                foreach (var aidizas in ids)
                {
                    if (aidizas.Equals(variant.Id.ToString()))
                    {
                        a.Add(variant.Variantas);
                    }
                }

            }
            StringBuilder builder = new StringBuilder();
            foreach (string aa in a) // Loop through all strings
            {
                builder.Append(aa).Append(","); // Append string to StringBuilder
            }
            string result = builder.ToString();
            var newresult  = result.Substring(0, result.Length - 1);
            List<string> abc = new List<string>();
            foreach (var item in a)
            {
                abc.Add(item);
            }
            ViewBag.Ats = abc;
           
            ViewBag.Id = id;
            var kazkas = form["variantas"];
            ViewBag.Kls = kazkas.Split(',');
            if (newresult.Equals(kazkas))
            {
                TempData["teisingas"] = "Tesiginai";
            }
            else
            {
                TempData["neteisingas"] = "Neteisingai";
            }
            return View();
        }
    }
}