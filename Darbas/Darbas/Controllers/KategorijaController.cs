﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Darbas.Controllers
{
    public class KategorijaController : Controller
    {
        DuombazeEntities db = new DuombazeEntities();
        // GET: Kategorija
        public ActionResult Sarasas()
        {
            List<Kategorija> kategorijos = db.Kategorijas.ToList();
                return View(kategorijos);
        }

        [HttpGet]
        public ActionResult Detales(int id)
        {
            Kategorija kategorija = db.Kategorijas.Single(kat => kat.Id == id);
            return View(kategorija);
        }
    }
}